﻿using System.Collections.Generic;
using UnityEngine;

public class AnimationProperties : MonoBehaviour
{
    public bool IsPlayOnce = false;
    public bool IsReversible = false;
    public bool IsResetWhenChangeVariation = true;
    public bool canAnimate = false;

    public List<GameObject> childAnimationObjects = new List<GameObject>();
    [System.NonSerialized]
    public List<Animation> childAnimationObjectAnimations = new List<Animation>();
    [System.NonSerialized]
    public List<AnimationProperties> childAnimationObjectAnimationProperties = new List<AnimationProperties>();

    [System.NonSerialized]
    public bool isPlayedOnce = false;
    [System.NonSerialized]
    public bool isAnimating = false;

    void Start()
    {
        foreach (GameObject childAnimationObject in childAnimationObjects)
        {
            childAnimationObjectAnimations.Add(childAnimationObject.GetComponent<Animation>());
            childAnimationObjectAnimationProperties.Add(childAnimationObject.GetComponent<AnimationProperties>());
        }
    }

    public void ToggleCanAnimate()
    {
        canAnimate = !canAnimate;
    }
}
