﻿using UnityEngine;
using System.Collections;

public class PointCloubDetection : MonoBehaviour {


	public TangoPointCloud pointCloub;
	public int number;
	public int boundaryNumberForPointCloub;

	// Update is called once per frame
	void Update () 
	{
		NumberOfPointCloubAvailabeNow();
	}



	public void NumberOfPointCloubAvailabeNow ()
	{

		number = pointCloub.m_pointsCount;
	}

	public bool isBoundaryReach ()
	{
		if (number < boundaryNumberForPointCloub) {
			return false;
		} else {
			return true;
		}

	}
}
