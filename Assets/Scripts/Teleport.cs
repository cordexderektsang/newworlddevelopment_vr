﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Teleport : MonoBehaviour {

	public List<GameObject> teleportSpot;
	public GameObject houseObject;
	public static Vector3 orignalPosition; 
	private int i = 0 ;
	public bool isTest = true;
	// Update is called once per frame

	public void Start (){
		if (isTest) 
		{
			GlobalStaticVariableForHouseDemo.SetUpHouseDefaultPosition = houseObject.transform.localPosition;
			isTest = false;
		}

	}

	public void testTeleport ()
	{

		if (i < teleportSpot.Count) {
			houseObject.transform.position = new Vector3 (GlobalStaticVariableForHouseDemo.SetUpHouseDefaultPosition.x + (teleportSpot [i].GetComponent<TeleportSpotSetUP> ().spotPosition.x * -1), GlobalStaticVariableForHouseDemo.SetUpHouseDefaultPosition.y, GlobalStaticVariableForHouseDemo.SetUpHouseDefaultPosition.z + (teleportSpot [i].GetComponent<TeleportSpotSetUP> ().spotPosition.z * -1));
			Debug.Log (GlobalStaticVariableForHouseDemo.SetUpHouseDefaultPosition +  " : " + teleportSpot [i].GetComponent<TeleportSpotSetUP> ().spotPosition);
			i++;
		} else {
			i = 0;
			houseObject.transform.position = new Vector3 (GlobalStaticVariableForHouseDemo.SetUpHouseDefaultPosition.x + (teleportSpot [i].GetComponent<TeleportSpotSetUP> ().spotPosition.x * -1), GlobalStaticVariableForHouseDemo.SetUpHouseDefaultPosition.y, GlobalStaticVariableForHouseDemo.SetUpHouseDefaultPosition.z + (teleportSpot [i].GetComponent<TeleportSpotSetUP> ().spotPosition.z * -1));
			Debug.Log (GlobalStaticVariableForHouseDemo.SetUpHouseDefaultPosition + " : " + teleportSpot [i].GetComponent<TeleportSpotSetUP> ().spotPosition);
		}
	}

	public void Update ()
	{
		if (Input.GetMouseButton (0)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, 1000)) {
				if (hit.collider.gameObject.name.Contains ("teleport")) {

					CCScreenLogger.LogStatic("hosue before " + houseObject.transform.position, 5);
					houseObject.transform.position = new Vector3 (GlobalStaticVariableForHouseDemo.SetUpHouseDefaultPosition.x + (hit.collider.gameObject.GetComponent<TeleportSpotSetUP>().spotPosition.x*-1),GlobalStaticVariableForHouseDemo.SetUpHouseDefaultPosition.y ,GlobalStaticVariableForHouseDemo.SetUpHouseDefaultPosition.z +(hit.collider.gameObject.GetComponent<TeleportSpotSetUP>().spotPosition.z*-1));
					CCScreenLogger.LogStatic ("SetUpHouseDefaultPosition " + GlobalStaticVariableForHouseDemo.SetUpHouseDefaultPosition ,7);
					CCScreenLogger.LogStatic("hosue after " + houseObject.transform.position, 6);
				}
			}
		}

	}
}
