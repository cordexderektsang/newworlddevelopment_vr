﻿using UnityEngine;
using System.Collections;

public class WebTexture : MonoBehaviour {

	public Renderer rend;
	public RenderTexture texture;
	public Camera tangoCam;
	// Use this for initialization
	void Start () 
	{
		tangoCam.targetTexture = texture;
		rend = GetComponent<Renderer>();
		rend.material.mainTexture = texture;
	}
	

}
