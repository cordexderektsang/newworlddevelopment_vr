﻿using UnityEngine;
using System.Collections;

public class LogDisplay : MonoBehaviour {
	public GameObject houseObject;
	public GameObject currentCam;
	public GameObject tangoCam;
	public TangoPointCloud pointCloub;
	public PointCloubDetection detect;
	public pointToDepth pointDepth;
	public GameObject warning;
	public GameObject areaWarning;
	public float boundaryDistance= 0.5f;
	public int boundaryPointCloud= 5000;
	public UITexture warningTexture;
	private float timeForDistance = 0;
	private float maxTimeDistance = 1f;
	private float timeForArea = 0;
	private float maxTimeArea = 1.5f;
	public float timeForInital = 0;
	public float maxTimeInital = 4f;
	public float timeForTango = 0;
	public float maxTimeTangol = 6f;
	public float timeForSignal = 0;
	private float alphaValue = 0f;
	private float incrementAlphaValue = 0.15f;
	public float delayTime = 1.5f;
	public GameObject placeUI;


	void Start ()
	{
		GlobalStaticVariableForHouseDemo.IsReachLimitDistance = false;
		GlobalStaticVariableForHouseDemo.IsReachLimitPointCloub = false;
		//StartCoroutine (WaitForTangoToBeReady());
		counterForTracking();
	}
	// Update is called once per frame
	void Update () 
	{
		//CCScreenLogger.LogStatic("houseObject position : " + houseObject.transform.position + "houseObject.rotation: " + houseObject.transform.rotation, 0);
		//CCScreenLogger.LogStatic("currentCam.position: " + currentCam.transform.position + "currentCam.rotation: " + currentCam.transform.rotation, 1);
		//CCScreenLogger.LogStatic("tangoCam.position: " + tangoCam.transform.position + "tangoCam.rotation: " + tangoCam.transform.rotation, 2);
		CCScreenLogger.LogStatic("pointCloub.Count: " + pointCloub.m_pointsCount , 3);
		CCScreenLogger.LogStatic("distance: " + pointDepth.distance(), 4);
		CCScreenLogger.LogStatic("is it ready: " + GlobalStaticVariableForHouseDemo.enoughCloub, 5);
		/*if (GlobalStaticVariableForHouseDemo.enoughCloub) {
			counterForArea ();
		} 
		//else {
			//counterForTracking ();
		//}*/
		if (!GlobalStaticVariableForHouseDemo.enoughCloub) 
		{
			counterForTracking ();
		} else if(GlobalStaticVariableForHouseDemo.enoughCloub )
		{
			counterForArea ();
			counterForDistance ();
			SetAlphaForWarningTexture ();

		}
	}



	IEnumerator WaitForTangoToBeReady (){

		yield return new WaitForSeconds (10);
		GlobalStaticVariableForHouseDemo.enoughCloub = true;
		Debug.Log ("-----------Tango------ready-------");

	}

	public void counterForTracking ()
	{

		if (timeForTango < maxTimeTangol) 
		{

			timeForTango += Time.deltaTime;
		} else {
			if (timeForInital < maxTimeInital) 
			{
				if (CheckPointCloud () && !areaWarning.activeSelf) 
				{
					timeForInital += Time.deltaTime;
				
				} else {
					areaWarning.SetActive (true);
					timeForDistance = 0;
				}
			} else {
				if (!GlobalStaticVariableForHouseDemo.enoughCloub) 
				{
					GlobalStaticVariableForHouseDemo.enoughCloub = true;
					Debug.Log ("-----------Tango------ready-------");
					placeUI.SetActive (true);
				} else {
					return;
				}
			}
		}
	}


	public void counterForArea ()
	{
		if (timeForArea < maxTimeArea && !GlobalStaticVariableForHouseDemo.IsReachLimitPointCloub) {
			if (!CheckPointCloud ()) {
				timeForArea += Time.deltaTime;
			} else {
				//GlobalStaticVariableForHouseDemo.IsReachLimitPointCloub = false;
				timeForArea = 0;
			}
		} else {
			areaWarning.SetActive (true);
			GlobalStaticVariableForHouseDemo.IsReachLimitPointCloub = true;
			timeForArea = 0;

		}
	}

	public void counterForDistance ()
	{
		if (timeForDistance < maxTimeDistance && !GlobalStaticVariableForHouseDemo.IsReachLimitDistance ) {
			if (!CheckDistance()) {
				timeForDistance += Time.deltaTime;
			} else {
				timeForDistance = 0;

			}
		} else {
			areaWarning.SetActive (true);
			GlobalStaticVariableForHouseDemo.IsReachLimitDistance = true;
			timeForDistance = 0;
			
		}

	}


	private bool CheckPointCloud ()
	{
		if ( pointCloub.m_pointsCount < boundaryPointCloud) 
		{
			return false;

		}
		return true;

	}

	private bool CheckDistance ()
	{

		if (pointDepth.distance () < boundaryDistance) 
		{
			return false;
		} 
		return true;
	}



	public void TurnOffWarningObject (GameObject target)
	{

		Debug.Log ("offTarget");
		target.SetActive (false);
		timeForDistance = 0;
		timeForArea = 0;
		GlobalStaticVariableForHouseDemo.IsReachLimitPointCloub = false;
		GlobalStaticVariableForHouseDemo.IsReachLimitDistance = false;

	}




	public float valueForDistance ()
	{

		float value = pointDepth.distance () / boundaryDistance;
		return value;


	}

	public float valueForPointCloud()
	{

		float value = pointCloub.m_pointsCount / boundaryPointCloud;
		return value;

	}

	public void SetAlphaForWarningTexture ()
	{
		float setValue = 0;
		string takeValue = "";
		float distance = valueForDistance ();
		float point = valueForPointCloud ();

		if (distance > point) 
		{
			setValue = point;
			takeValue = "pointCloud";
		} else {
			setValue = distance;
			takeValue = "distance";
		}

		CCScreenLogger.LogStatic("value : " +setValue +" d" + distance + " p" + point, 2);
		//StartCoroutine (AlphaEffect(setValue,0.1f));
		if (pointDepth.distance() >= 3.5f && pointCloub.m_pointsCount >= boundaryPointCloud)
		{
			warningTexture.color = new Color (1, 1, 1, 0);
			alphaValue = 0;
			timeForSignal = 0;
			CCScreenLogger.LogStatic("Distance Far", 0);
		} else if (setValue <= 1)
		{
			warningTexture.color = new Color (1, 1, 1, 1);
			alphaValue = 0;
			timeForSignal = 0;
			CCScreenLogger.LogStatic("Too Closet", 0);
		} else 
		{
			float AdditionValue = -2.1f;
			//hard core value
			if (setValue < 4 + AdditionValue) {
				if (setValue > 3.9f + AdditionValue && setValue < 4 + AdditionValue) {
					//incrementAlphaValue = 0.01f;
					incrementAlphaValue = 0.005f;

				} else if (setValue > 3.8f + AdditionValue && setValue <= 3.9f + AdditionValue) {
					//incrementAlphaValue = 0.02f;
					incrementAlphaValue = 0.01f;

				} else if (setValue > 3.6f + AdditionValue && setValue <= 3.7f + AdditionValue) {
					//incrementAlphaValue = 0.02f;
					incrementAlphaValue = 0.015f;

				}  else if (setValue > 3.5f + AdditionValue && setValue <= 3.6f + AdditionValue) {
						//incrementAlphaValue = 0.02f;
						incrementAlphaValue = 0.025f;

				} else if (setValue > 3.4f + AdditionValue && setValue <= 3.5f + AdditionValue) {
					//incrementAlphaValue = 0.02f;
					incrementAlphaValue = 0.03f;

				} else if (setValue > 3.3f + AdditionValue && setValue <= 3.4f + AdditionValue) {
					//incrementAlphaValue = 0.02f;
					incrementAlphaValue = 0.04f;

				} else if (setValue > 3.2f + AdditionValue && setValue <= 3.3f + AdditionValue) {
					//incrementAlphaValue = 0.02f;
					incrementAlphaValue = 0.07f;

				}else if (setValue > 3.1f + AdditionValue && setValue <= 3.2f + AdditionValue) {
					//incrementAlphaValue = 0.02f;
					incrementAlphaValue = 0.1f;

				}
		
				AlphaEffects ();
			} else {

				warningTexture.color = new Color (1, 1, 1, 0);
				alphaValue = 0;
				timeForSignal = 0;
				CCScreenLogger.LogStatic("setValue Far", 0);
			}
		}

	}


	void AlphaEffects()
	{
		if (alphaValue < 1) 
		{
			warningTexture.color = new Color (1, 1, 1, alphaValue);
			alphaValue += incrementAlphaValue;
			CCScreenLogger.LogStatic(" incrementAlphaValue: " + incrementAlphaValue, 0);
		} else {
			alphaValue = 0;
	
		}
	

	}

	/*void AlphaEffects(float delayTime ßfloat incrementAlphaValue)
	{

		if (timeForSignal < delayTime) {
			if (alphaValue < 1) {
				warningTexture.color = new Color (1, 1, 1, alphaValue);
				timeForSignal += Time.deltaTime;
				alphaValue += incrementAlphaValue;
			}
			alphaValue = 0;
		} else {
			warningTexture.color = new Color (1,1,1,0);
			alphaValue = 0;
			timeForSignal = 0;
		}

	}

	IEnumerator AlphaEffect (float delayTime , float incrementAlphaValue)
	{

		float thisincrementAlphaValue = incrementAlphaValue;
		while (true)
		{
			yield return new WaitForSeconds (delayTime);
			if (warningTexture.color.a < 1) {
				warningTexture.color = new Color (1,1,1,thisincrementAlphaValue);
				thisincrementAlphaValue += incrementAlphaValue;
			} else{

				break;

			}

		}

	}*/

		
}
